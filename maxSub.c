//CS 325 - Assignment 2
//Nick Alto  - compile with 'gcc -o maxSub maxSub.c'
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

FILE *fp = NULL;
#define DEBUG 0
typedef int (*fptr)(int*, int);

struct dpTable {
	int curVal;
	int maxVal;
};

int algC(int*p, int a, int b);

void parseFile (char *file, fptr functionPtr) {
	char line[2048];
	// doesn't handle extremely large datasets buffer is at 2048 currently
	// for larger datasets use manual testing
	clock_t t;
	char *token = NULL;
	char seps[] = ", ";
	int *input = (int *)malloc(1024*(sizeof(int)));
	int i = 0, var = 0, ret=0;

	fp = fopen(file, "rt");
	if( fp != NULL ) {
		while( fgets(line, sizeof(line), fp) != NULL ) {
			i = 0;
			//parse into array, call algorithm
			if(DEBUG)
				printf("line = %s\n", line);
		   			
			token = strtok(line+1, seps);
			while (token != NULL) {
    			sscanf (token, "%d", &var);
    			token = strtok(NULL, seps);
                if (token)
					input[i++] = var;
			}
			if(strlen(line) > 1) {
			    t = clock(); 
				if(functionPtr)
					ret = functionPtr(input, i);
				else 
					ret = algC(input, 0, i-1);
				t = clock() - t;
				(ret == var) ? printf("PASSED %d == %d (%f sec)\n", var, ret, ((float)t/CLOCKS_PER_SEC)) : printf("FAILED %d (%f sec)\n", var, ((float)t/CLOCKS_PER_SEC));
			}
		}
		fclose(fp);
	} else {
		perror(file);
	}
}

int* arrGen(int size, int seed) {
	int i = 0, num1 = 0, num2 = 0; 
	int *arr = (int *)malloc(sizeof(int)*size);
	srand(time(NULL)/seed);
	if(DEBUG)
		printf("[");
	for (; i<size; i++) {
			num1 = rand() % 100;
			num2 = rand() % 100;
			if(DEBUG)
				printf("%d, ", num1-num2);
			arr[i] = (num1 - num2);
	}
	if(DEBUG)
		printf("]\n");
	return arr;
}

int algA (int *input, int count) {
	int i=0, j=0, max_sum=input[0], sum=0; 

	for(i=0; i<count; i++) {
		for(j=i; j<count; j++) {
            sum += input[j];
			if( sum > max_sum )
				max_sum = sum;
		}
		sum = 0;
	}
	return max_sum;
}

int algB (int *input, int count) {
	int i=0, max_sum=input[0], sum=0;
	
	for(i=0; i<count; i++) {
		(sum <= 0 ) ? (sum = input[i]) : (sum += input[i]);	

		if(sum > max_sum)
			max_sum = sum;
	}
	return max_sum;
}


int max(int left, int right, int mid) {
	if( left >= right && left >= mid)
		return left;
	else if (right >= left && right >= mid) 
		return right;
	else if ( mid >= right && mid >= left)
		return mid;
}

int midSum(int *a, int count, int cur, int flag) {
	int sum=0, i=cur, max_sum;
	max_sum = a[cur];
	if( flag ) {
    	for(; i>= count; i--) {
			sum += a[i];
			if (sum > max_sum)
				max_sum = sum;
		}
	} else {
		for(; i <= count; i++) {
			sum += a[i];
			if (sum > max_sum) 
				max_sum = sum;
		}
	}
	return max_sum;
}

int algC (int *a, int low, int high) {
	if(low == high) 
		return a[low];

	int mid = (low+high)/2;
	int leftSum = algC(a, low, mid);
	int rightSum = algC(a, mid+1, high);
	int middleSum = midSum(a, low, mid, 1) + midSum(a, high, mid+1,  0);
	return max(leftSum, rightSum, middleSum);
}

struct dpTable dpBase (struct dpTable dpT, int baseVal) {
	dpT.curVal = 0;
	dpT.maxVal = baseVal;
	return dpT;
}

struct dpTable algDpc (int *a, int count) {
	struct dpTable dpT;
	//recurse until we reach base case 
	dpT = (count <= 1) ? ((count < 1) ? dpBase(dpT, 0) : dpBase(dpT, a[0])) : algDpc(a, count-1);
	
	//iteratively add each element and store curVal and maxVal
	dpT.curVal += a[count-1];
	dpT.curVal = (dpT.curVal < 0) ? 0 : dpT.curVal;
	dpT.maxVal = (dpT.curVal > dpT.maxVal) ? dpT.curVal : dpT.maxVal;
	return dpT;
}

int algDp (int *a, int count) {
	struct dpTable dpT;
	dpT = algDpc(a, count);
	return dpT.maxVal;
}

void testing(int arg) {
	int j=0, i=0, retA=0, retB=0, retC=0, retDp=0;
	float trA=0, trB=0, trC=0, trDp=0;
	int *arr;
	clock_t tA, tB, tC, tDp;
	for(;i<100; i++,j=0) {
		arr = arrGen(arg, i+1);
		tA = clock();
		retA = algA(arr, arg);
		tA = clock() - tA;
		tB = clock();
		retB = algB(arr, arg);
		tB = clock() - tB;
		tC = clock();
		retC = algC(arr, 0, arg-1);
		tC = clock() - tC;
		tDp = clock();
		retDp = algDp(arr, arg);
		tDp = clock() - tDp;
		trA += (float)tA/CLOCKS_PER_SEC;
		trB += (float)tB/CLOCKS_PER_SEC;
		trC += (float)tC/CLOCKS_PER_SEC;
		trDp += (float)tDp/CLOCKS_PER_SEC;
		if (retA == retB && retB == retC && retC == retDp) {
            printf("PASSED (algA %f sec - algB %f sec - algC %f sec - algDp %f sec)\n",
            ((float)tA/CLOCKS_PER_SEC), ((float) tB/CLOCKS_PER_SEC), ((float)tC/CLOCKS_PER_SEC), 
			((float)tDp/CLOCKS_PER_SEC));
        } else {
			printf("FAILED (algA %f sec - algB %f sec - algC %f sec - algDp %f sec)\n", 
			((float)tA/CLOCKS_PER_SEC), ((float) tB/ CLOCKS_PER_SEC), ((float)tC/CLOCKS_PER_SEC),
			((float)tDp/CLOCKS_PER_SEC));
			printf("[");
			for(; j< arg; j++)
				printf("%d, ", arr[j]);
			printf("]\n");
			printf("retA = %d != retB = %d != retC = %d != retDp = %d\n", retA, retB, retC, retDp);
		}
		free(arr);
	}
	printf("algA total time (%d iterations of size %d) = %f\n", i, arg, trA/(float)i);
    printf("algB total time (%d iterations of size %d) = %f\n", i, arg, trB/(float)i);
    printf("algC total time (%d iterations of size %d) = %f\n", i, arg, trC/(float)i);
	printf("algDp total time (%d iterations of size %d) = %f\n", i, arg, trDp/(float)i);
}

void DPTesting(int arg) {
    int j=0, i=0, retC=0, retDp=0;
    float trC=0, trDp=0;
    int *arr;
    clock_t tC, tDp;
    for(;i<100; i++,j=0) {
        arr = arrGen(arg, i+1);
        tC = clock();
        retC = algC(arr, 0, arg-1);
        tC = clock() - tC;
        tDp = clock();
        retDp = algDp(arr, arg);
        tDp = clock() - tDp;
        trC += (float)tC/CLOCKS_PER_SEC;
        trDp += (float)tDp/CLOCKS_PER_SEC;
        if(retC != retDp) {
            printf("FAILED (algC %f sec - algDp %f sec)\n",
            ((float)tC/CLOCKS_PER_SEC), ((float)tDp/CLOCKS_PER_SEC));
            printf("[");
            for(; j< arg; j++)
                printf("%d, ", arr[j]);
            printf("]\n");
            printf("retC = %d != retDp = %d\n", retC, retDp);
        }

        free(arr);
    }
    	//printf("algC  total time (%d iterations of size %d) = %f\n", i, arg, trC/(float)i);
    	//printf("algDp total time (%d iterations of size %d) = %f\n", i, arg, trDp/(float)i);
		printf("difference %d = %f\n", i, ((trC/(float)i)-(trDp/(float)i)));
}

int main (int argc, char *argv[]) {

	int *arr = NULL; 
	int ret = 0, i=0, arg=0;
	fptr functionPtr = NULL;
	if( argc < 2 ) {
		printf("ERROR:Invalid Usage\n");
		printf("USAGE:		[./maxSub -f <filename>]	:Input from file\n		[./maxSub -m]			:Manual testing\n		[./maxSub -t <size>]			:Autogenerated Testing\n");
	} else {
		if(strncmp(argv[1], "-f", 2) == 0 && argc >= 3 ) {
			functionPtr = &algA;
			printf("algA\n");
            parseFile(argv[2], functionPtr);
			functionPtr = &algB;
			printf("algB\n");
			parseFile(argv[2], functionPtr);
			functionPtr = NULL;
			printf("algC\n");
			parseFile(argv[2], functionPtr);
		} else if (strncmp(argv[1], "-m", 2) == 0) {
			printf("manual testing\n");
			 //manual testing
			
    		clock_t tB, tC;
			arr = arrGen(10000000, 2);
			tB = clock();
			ret = algB(arr, 10000000);
			tB = clock() - tB;
			printf("retB = %d in %f\n", ret, (float)tB/CLOCKS_PER_SEC);
 			tC = clock();
            ret = algC(arr, 0, 9999999);
            tC = clock() - tC;
            printf("retB = %d in %f\n", ret, (float)tC/CLOCKS_PER_SEC);
			/*
			int ar[5] = {-11, -200, -33, -7, -50};
	        ret = algA(ar, 5);
			printf("retA = %d\n", ret);
			ret = algB(ar, 5);
			printf("retB = %d\n", ret);
			ret = algC(ar, 0, 4);
			printf("retC = %d\n", ret);
		   // */
		} else if (strncmp(argv[1], "-t", 2) == 0 && argc >= 3) {
			printf("testing...\n");
			arg = atoi(argv[2]);
			testing(arg);
			printf("end of testing\n");
		} else if (strncmp(argv[1], "-dp", 2) == 0 && argc >= 2) {
        	printf("DPTesting\n");
			DPTesting(100);
			DPTesting(500);
			DPTesting(1000);
			DPTesting(5000);
			DPTesting(10000);
			DPTesting(15000);
			DPTesting(25000);
			DPTesting(50000);
			DPTesting(75000);
			DPTesting(100000);
			printf("end of DPTesting\n");
		}
	}
  return 0;
}
